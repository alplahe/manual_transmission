﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalogueSpeedConverter : MonoBehaviour
{
	private static float m_minimumAngle = 180.0f;
	private static float m_maximumAngle = 0.0f;
	private static AnalogueSpeedConverter m_speedConverter;
	[SerializeField] private GameObject m_gearbox;

	// Use this for initialization
	void Start ()
	{
		m_speedConverter = this;
		m_gearbox.GetComponent<IGearbox>().OnShiftGears += HandleOnShiftGears;
	}

	public static void ShowSpeed(float speed, float minimumSpeed, float maximumSpeed)
	{
		float speedPercent = Mathf.InverseLerp(minimumSpeed, maximumSpeed, speed);
		float angle = Mathf.Lerp(m_minimumAngle, m_maximumAngle, speedPercent);
		m_speedConverter.transform.eulerAngles = new Vector3(0, 0, angle);
	}
	
	public void HandleOnShiftGears(int gear)
	{
		m_minimumAngle = 180;
//		m_maximumAngle = m_gearbox.GetComponent<IGearbox>().GetGearMaximumSpeed(gear);
		m_maximumAngle = 0;
	}
}
