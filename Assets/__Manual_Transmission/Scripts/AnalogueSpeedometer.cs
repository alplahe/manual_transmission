﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Speedometer/Analogue", fileName = "AnalogueSpeedometer")]
public class AnalogueSpeedometer : ScriptableObject
{
//	[SerializeField] private Sprite m_speedometerSprite;
//	[SerializeField] private Sprite m_needleSprite;
	public Canvas m_canvasSpeedometer;
	public float m_minimumAngle;
	public float m_maximumAngle;
}
