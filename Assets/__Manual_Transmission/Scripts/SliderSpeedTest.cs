﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderSpeedTest : MonoBehaviour
{
	private Slider m_slider;
	[SerializeField] private AnalogueSpeedometer m_analogueSpeedometer;
	[SerializeField] private GameObject m_gearbox;
	
	private float m_minimumSpeed = 0.0f;
	private float m_maximumSpeed = 100.0f;
	
	private const float m_MinimumSpeedOfSpeedometer = 0.0f;
	private const float m_MaximumSpeedOfSpeedometer = 100.0f;

	// Use this for initialization
	void Start ()
	{
		m_slider = GetComponent<Slider>();

		m_gearbox.GetComponent<IGearbox>().OnShiftGears += HandleOnShiftGears;
		InitializeEngineInFirstGear();
	}

	public void HandleOnShiftGears(int gear)
	{
		m_minimumSpeed = m_gearbox.GetComponent<IGearbox>().GetGearMinimumSpeed(gear - 1);
		m_maximumSpeed = m_gearbox.GetComponent<IGearbox>().GetGearMaximumSpeed(gear - 1);
	}

	private void InitializeEngineInFirstGear()
	{
		m_minimumSpeed = m_gearbox.GetComponent<IGearbox>().GetGearMinimumSpeed(0);
		m_maximumSpeed = m_gearbox.GetComponent<IGearbox>().GetGearMaximumSpeed(0);
	}

	// Update is called once per frame
	void Update ()
	{
		AnalogueSpeedConverter.ShowSpeed(Speed, m_MinimumSpeedOfSpeedometer, m_MaximumSpeedOfSpeedometer);
	}

	private float Speed
	{
		get { return m_minimumSpeed + m_slider.value * (m_maximumSpeed - m_minimumSpeed); }
	}
}
