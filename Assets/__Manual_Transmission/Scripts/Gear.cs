﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Gear", fileName = "Gear")]
public class Gear : ScriptableObject
{
	public float m_minimumSpeed;
	public float m_maximumSpeed;
}
