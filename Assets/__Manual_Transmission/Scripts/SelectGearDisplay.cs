﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectGearDisplay : MonoBehaviour
{
	[SerializeField] private GearDisplay m_gearDisplay;
	[SerializeField] private GearButtons m_gearButtons;
	private Image m_image;

	private void Start()
	{
		m_image = GetComponent<Image>();
		m_gearButtons.GetComponent<GearButtons>().OnShiftGears += HandleOnShiftGears;
	}

	public void HandleOnShiftGears(int gear)
	{
		m_image.sprite = m_gearDisplay.m_sprites[gear - 1];
	}
}
