﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GearButtons : MonoBehaviour, IGearbox
{
	[SerializeField] private Engine m_engine;
	[SerializeField] private Button m_buttonPrefab;
	private int m_numberOfGears;

	public event Action<int> OnShiftGears = delegate {};
	
	public float GetGearMinimumSpeed(int gearNumber)
	{
		return m_engine.GetGearMinimumSpeed(gearNumber);
	}

	public float GetGearMaximumSpeed(int gearNumber)
	{
		return m_engine.GetGearMaximumSpeed(gearNumber);
	}

	// Use this for initialization
	void Start ()
	{
		CreateButtons();
	}

	private void CreateButtons()
	{
		m_numberOfGears = m_engine.NumberOfGears;
		for (int i = 0; i < m_numberOfGears; i++)
		{
			Button button = Instantiate(m_buttonPrefab);
			SetThisGameObjectAsButtonParent(button);
			SetButtonText(button, i);
			
			AddListenerOnShiftGearsToButton(button);
		}
	}

	private void AddListenerOnShiftGearsToButton(Button button)
	{
		int buttonNumber = GetButtonTextNumber(button);
		button.onClick.AddListener(() => OnShiftGears(buttonNumber));
	}

	private void SetThisGameObjectAsButtonParent(Button button)
	{
		button.transform.SetParent(this.transform, true);
	}

	private static void SetButtonText(Button button, int i)
	{
		button.GetComponentInChildren<Text>().text += (i + 1).ToString();
	}

	private static int GetButtonTextNumber(Button button)
	{
		return int.Parse(GetNumberOfButtonFromText(button));
	}

	private static string GetNumberOfButtonFromText(Button button)
	{
		return button.GetComponentInChildren<Text>().text.Substring(GetIndexOfLastCharacterOfTheButtonText(button));
	}

	private static int GetIndexOfLastCharacterOfTheButtonText(Button button)
	{
		return button.GetComponentInChildren<Text>().text.Length-1;
	}
}
