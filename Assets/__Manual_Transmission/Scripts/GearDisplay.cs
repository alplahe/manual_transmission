﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "GearDisplay", fileName = "GearDisplay")]
public class GearDisplay : ScriptableObject
{
	public Sprite[] m_sprites;
}
