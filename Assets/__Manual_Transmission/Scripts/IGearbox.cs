﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGearbox
{
	event Action<int> OnShiftGears;
	float GetGearMinimumSpeed(int gearNumber);
	float GetGearMaximumSpeed(int gearNumber);
}
