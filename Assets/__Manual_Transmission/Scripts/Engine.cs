﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Engine", fileName = "Engine")]
public class Engine : ScriptableObject
{
	public Gear[] m_gears;
	public float m_minimumSpeed;
	public float m_maximumSpeed;

	public int NumberOfGears
	{
		get { return m_gears.Length; }
	}

	private void OnValidate()
	{
		GetMinimumAndMaximumSpeeds();
	}

	public void GetMinimumAndMaximumSpeeds()
	{
		m_minimumSpeed = 0;
		m_maximumSpeed = 0;
		for (int i = 0; i < m_gears.Length; i++)
		{
			SetMinimumSpeed(i);
			SetMaximumSpeed(i);
		}
	}

	private void SetMinimumSpeed(int i)
	{
		float minimumSpeedFromGear = m_gears[i].m_minimumSpeed;
		if (minimumSpeedFromGear < m_minimumSpeed)
		{
			m_minimumSpeed = minimumSpeedFromGear;
		}
	}

	private void SetMaximumSpeed(int i)
	{
		float maximumSpeedFromGear = m_gears[i].m_maximumSpeed;
		if (maximumSpeedFromGear > m_maximumSpeed)
		{
			m_maximumSpeed = maximumSpeedFromGear;
		}
	}

	public float GetGearMinimumSpeed(int gearNumber)
	{
		return m_gears[gearNumber].m_minimumSpeed;
	}

	public float GetGearMaximumSpeed(int gearNumber)
	{
		return m_gears[gearNumber].m_maximumSpeed;
	}
}
